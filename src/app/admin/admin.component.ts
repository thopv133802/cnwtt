import { Component, OnInit } from '@angular/core';
import {select, Store} from "@ngrx/store"
import {fetchNewses, RootState} from "../app.reducer"
import {NewsService} from "../data/services/news.service"
import {News} from "../data/models/news.model"
import {Observable} from "rxjs"
import {Router} from "@angular/router"
import {MatSnackBar} from "@angular/material"

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  newses: News[]
  searchedNewses: News[]
  constructor(private newsService: NewsService, private router: Router, private snackbar: MatSnackBar) { }

  ngOnInit() {
    this.newsService.fetchNewses().subscribe(newses =>{
      this.newses = newses
      this.searchedNewses = [...newses]
    })
  }

  onUpdateButtonClicked(news: News) {
    this.newsService.updateNews(news)
      .subscribe(null, error => {
        this.showMessage(error)
      }, () => {
        this.showMessage("Cập nhật thành công")
      })
  }


  showMessage(message: string){
    this.snackbar.open(message, null, {
      duration: 2000
    })
  }

  onSearchButtonClick(keyword: string = ""){
    this.searchedNewses = this.newses.filter(news => {
      return news.title.toLowerCase().includes(keyword.toLowerCase())
    })
  }

  navigateToCreateNews() {
    this.router.navigate(["/create-news"])
  }
}
