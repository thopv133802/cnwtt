import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router"
import {Location} from "@angular/common"
import {NewsService} from "../../data/services/news.service"
import {MatSnackBar} from "@angular/material"
import {first} from "rxjs/operators"
@Component({
  selector: 'app-create-news',
  templateUrl: './create-news.component.html',
  styleUrls: ['./create-news.component.css']
})
export class CreateNewsComponent implements OnInit {
  authorText = "Theo Chi Chi (Khám phá)."
  contentText = "Béo phì là nỗi ám ảnh của không ít người. Nhưng đừng quá hoang mang, bởi phương pháp giảm 21kg của hot mom Hà thành có thể sẽ là \"cứu cánh\" cho lớp mỡ thừa nhức nhối của chị em. Động lực nào khiến chị chia sẻ phương pháp giảm cân mà các mẹ vẫn hay nhắc là “phương pháp giảm cân thần kỳ\" lên các trang mạng xã hội?\n" +
    "\n" +
    "Mình tin rằng phương pháp của mình sẽ giúp giải quyết triệt để được nỗi lo mang tên béo phì của các mẹ, các chị.\n" +
    "\n" +
    "Nhiều người sẽ cho rằng chị chia sẻ bí quyết gia truyền với nhiều mục đích khác nhau, chị nghĩ sao về vấn đề này?\n" +
    "\n" +
    "Bản thân là một doanh nhân, mong muốn công việc buôn bán của mình ngày càng phát đạt là điều hiển nhiên, thế nhưng tiền bao nhiêu cũng không mua được lòng tin, phương châm sống của mình: chân thành tạo niềm tin nơi người khác. Chia sẻ chuyện giảm cân của bản thân, có sao nói vậy, đây bạn nhìn xem hình ảnh mình trước và sau giảm cân từ 69kg xuống 48kg trong 2 tháng sau khi sinh em bé đầu lòng.\n" +
    "\nHiện nay trên thị trường các chị em bị hoang mang bởi trăm nghìn cách giảm cân khác nhau, thậm chí các sản phẩm không rõ nguồn gốc xuất xứ nhan nhản, không những không giảm được cân mà còn làm ảnh hưởng tới sức khỏe, tinh thần của người dùng. Vậy chị có tự tin vào sản phẩm của mình có ưu điểm nổi trội nào hơn các sản phẩm khác?\n" +
    "\n" +
    "Liên quan đến sức khỏe tính mạng con người an toàn là yếu tố đặt lên hàng đầu thế nên Ngân đưa sản phẩm của mình đến các trung tâm nghiên cứu, kiểm nghiệm, đong đếm kĩ lưỡng mới đưa vào sử dụng. Tết vừa rồi cũng là cơ hội cho mình thử nghiệm lại một lần sản phẩm để ngày càng hoàn thiện “đứa con tinh thần”.\n" +
    "\n" +
    "Bạn thấy không Tết toàn món nhiều dầu mỡ, protein, lại toàn món ưa thích của Bi (tên ở nhà của Ngân Hồ) nên tăng 5kg, thế mà dùng thảo mộc đã ngót gần 3kg, mụn nhọt do thức khuya, thừa đạm cũng lặn hết, lại hồng hào như trước rồi này, và tuyệt nhiên thoải mái vận động mà không hề xuất hiện chút tác dụng phụ nào.\nChị có nghĩ phương pháp giảm cân của chị được nhiều mẹ đón nhận không?\n" +
    "\n" +
    "Ngân Hồ quan điểm thế này: mình mang phương pháp chia sẻ với mọi người và hình ảnh của mình phản ánh chân thực nhất sự thay đổi kì diệu trong cuộc sống sau khi giảm cân, xinh đẹp hơn.\n" +
    "\n" +
    "Ngân không lấy mình làm tiêu chuẩn hay hình mẫu để các mẹ noi theo, nhưng rõ ràng chị em chúng mình đẹp làm gì cũng trơn tru, thuận lợi hơn, thế nên Ngân tự tin phương pháp giảm cân này sẽ được đón nhận. Điều khiến Ngân vui nhất là nhận được phản hồi tích cực từ chị em sau khi giảm cân thành công.\nPhụ nữ chúng mình không đẹp kiểu Ngọc Trinh, nhưng cũng phải sexy, lả lơi với chồng theo cách của riêng mình. Và cách tốt nhất mà Ngân theo đuổi chính là đẹp lên mỗi ngày, phụ nữ không đẹp thì phải thơm, đặc biệt thân hình thon gọn săn chắc sẽ là vũ khí khiến đàn ông phải đắm đuối không rời nửa bước. Vì thế Ngân Hồ mong muốn phương pháp giảm cân bằng thảo mộc này sẽ là người bạn đồng hành cùng nhan sắc phụ nữ Việt!"
  thumbnailText = "https://cdn.eva.vn/upload/4-2018/images/2018-12-03/hotmom-ha-thanh-danh-bay-21kg-sau-sinh-voi-phuong-phap-giam-can-khong-phai-kho-so-1-1543820992-707-width600height900.jpg"
  titleText = "Hotmom Hà thành đánh bay 21kg sau sinh với phương pháp giảm cân \"không phải khổ sở\""

  constructor(private snackbar: MatSnackBar, private router: Router, private location: Location, private newsService: NewsService) { }

  ngOnInit() {
  }

  onCreateButtonClick() {
    this.newsService.addNews(
      {
          title: this.titleText,
          author: this.authorText,
          content: this.contentText,
          thumbnail: this.thumbnailText,
      }
    ).subscribe(null, error => {
      this.showMessage(error)
    }, () => {
      this.showMessage("Thêm thành công")
      this.clearFormFields()
    })
  }
  showMessage(message: string){
    this.snackbar.open(message, "Trang quản trị", {
      duration: 2000
    }).onAction().pipe(first())
      .subscribe(() => {
        this.router.navigate(["/admin"])
      })
  }
  clearFormFields(){
    this.authorText = null
    this.contentText = null
    this.thumbnailText = null
    this.titleText = null
  }
}
