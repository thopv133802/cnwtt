import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core"
import {News} from "../../data/models/news.model"

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {
  @Input()
  news: News
  @Output()
  editButtonClickListener = new EventEmitter<string>()
  constructor() { }

  ngOnInit() {
  }

  onEditButtonClicked(){
    this.editButtonClickListener.next(this.news.id)
  }
}
