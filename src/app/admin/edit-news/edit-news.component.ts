import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core"
import {News} from "../../data/models/news.model"
import {NewsService} from "../../data/services/news.service"
import {MatSnackBar} from "@angular/material"
import {ActivatedRoute, Router} from "@angular/router"
import {first} from "rxjs/operators"

@Component({
  selector: 'app-edit-news',
  templateUrl: './edit-news.component.html',
  styleUrls: ['./edit-news.component.css']
})
export class EditNewsComponent implements OnInit {
  @Input()
  news: News
  @Output()
  updateButtonClickListener = new EventEmitter<News>()
  constructor(private newsService: NewsService, private snackbar: MatSnackBar,  private router: Router) { }

  ngOnInit() {
  }

  onUpdateButtonClick() {
    this.updateButtonClickListener.next(this.news)
  }
}
