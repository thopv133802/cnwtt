import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core"

@Component({
  selector: 'app-appbar',
  templateUrl: './appbar.component.html',
  styleUrls: ['./appbar.component.css']
})
export class AppbarComponent implements OnInit {
  @Input()
  title = "Fournews"
  @Output()
  menuIconClick = new EventEmitter<void>()
  constructor() { }

  ngOnInit() {
  }

  onMenuIconClick() {
    this.menuIconClick.next()
  }
}
