import {Component, EventEmitter, OnDestroy, OnInit, Output} from "@angular/core"
import {Observable} from "rxjs"
import {Store, select} from "@ngrx/store"
import {fetchIsAuth, RootState} from "../../app.reducer"
import {AuthService} from "../../data/services/auth.service"
import {map} from "rxjs/operators"
import {Role} from "../../data/models/auth.model"

@Component({
  selector: "app-sidenav-list",
  templateUrl: "./sidenav-list.component.html",
  styleUrls: ["./sidenav-list.component.css"]
})
export class SidenavListComponent implements OnInit{
  @Output()
  itemClick: EventEmitter<void> = new EventEmitter<void>()
  isAuth$: Observable<boolean>
  isAdmin$: Observable<boolean>
  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.isAuth$ = this.authService.fetchIsAuth()
    this.isAdmin$ = this.authService.fetchCurrentUser()
      .pipe(
        map(user => user != null && user.role === Role.Admin)
      )
  }
  onItemClick() {
    this.itemClick.next()
  }
}
