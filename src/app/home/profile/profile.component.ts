import { Component, OnInit } from '@angular/core';
import {User} from "../../data/models/auth.model"
import {AuthService} from "../../data/services/auth.service"
import {MatSnackBar} from "@angular/material"

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  user: User
  constructor(private authService: AuthService, private snackbar: MatSnackBar) { }

  ngOnInit() {
    this.authService.fetchCurrentUser().subscribe(user => {
      this.user = user
    })
  }

  updateProfile() {
    this.authService.updateUser(this.user).subscribe(null, error => {
      this.showMessage(error)
    }, () => {
      this.showMessage("Cập nhật hồ sơ thành công")
    })
  }
  showMessage(message: string){
    this.snackbar.open(message, null, {
      duration: 2000
    });
  }
}
