import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core"
import {News} from "../../data/models/news.model"

@Component({
  selector: 'app-news-card',
  templateUrl: './news-card.component.html',
  styleUrls: ['./news-card.component.css']
})
export class NewsCardComponent implements OnInit {
  @Input()
  news: News
  @Output()
  itemDetailClick = new EventEmitter<string>()
  constructor() { }

  ngOnInit() {
  }

  onItemDetailClick() {
    this.itemDetailClick.next(this.news.id)
  }
}
