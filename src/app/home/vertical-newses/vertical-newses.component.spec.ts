import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerticalNewsesComponent } from './vertical-newses.component';

describe('VerticalNewsesComponent', () => {
  let component: VerticalNewsesComponent;
  let fixture: ComponentFixture<VerticalNewsesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerticalNewsesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerticalNewsesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
