import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core"
import {News} from "../../data/models/news.model"

@Component({
  selector: 'app-vertical-newses',
  templateUrl: './vertical-newses.component.html',
  styleUrls: ['./vertical-newses.component.css']
})
export class VerticalNewsesComponent implements OnInit {
  @Input()
  newses: News[]

  @Output()
  itemDetailClick = new EventEmitter<string>()
  constructor() { }

  ngOnInit() {
  }
  onItemDetailClick(id: string){
    this.itemDetailClick.next(id)
  }
}
