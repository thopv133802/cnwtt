import { Component, OnInit } from '@angular/core';
import {News} from "../data/models/news.model"
import {NewsService} from "../data/services/news.service"
import {Router} from "@angular/router"
import {routes} from "../app-routing.module"

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  newses: News[]

  constructor(private newsService: NewsService, private router: Router) {

  }

  ngOnInit() {
    this.newsService.fetchNewses()
      .subscribe(newses => {
        this.newses = newses
      })
  }

  onItemDetailClick($event: string) {
    console.log($event)
    this.router.navigate(["/news/", $event])
  }
}
