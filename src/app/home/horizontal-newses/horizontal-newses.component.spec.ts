import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HorizontalNewsesComponent } from './horizontal-newses.component';

describe('HorizontalNewsesComponent', () => {
  let component: HorizontalNewsesComponent;
  let fixture: ComponentFixture<HorizontalNewsesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HorizontalNewsesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HorizontalNewsesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
