import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core"
import {News} from "../../data/models/news.model"

@Component({
  selector: 'app-horizontal-newses',
  templateUrl: './horizontal-newses.component.html',
  styleUrls: ['./horizontal-newses.component.css']
})
export class HorizontalNewsesComponent implements OnInit {
  @Input()
  newses: News[]
  @Output()
  itemDetailClick = new EventEmitter<string>()
  constructor() { }

  ngOnInit() {
  }
  onItemDetailClick(itemID: string){
    this.itemDetailClick.next(itemID)
  }
}
