import {Component, OnInit} from "@angular/core"
import {fetchCurrentUser, RootState} from "./app.reducer"
import {select, Store} from "@ngrx/store"
import {catchError, defaultIfEmpty, map, take} from "rxjs/operators"
import {SetUnauthenticated} from "./data/redux/auth.action"
import {Observable, of} from "rxjs"
import {User} from "./data/models/auth.model"

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'Fournews';
  currentUser$: Observable<string>
  constructor(private store: Store<RootState>){}
  ngOnInit(): void {
    this.currentUser$ = this.store.pipe(
      select(fetchCurrentUser)
    ).pipe(
      map(user => {
        return user == null ? "Khách" : user.username
      })
    )
  }
}
