import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from "@angular/router"
import {AuthService} from "../data/services/auth.service"
import {Injectable} from "@angular/core"
import {RootState} from "../app.reducer"
import {Store} from "@ngrx/store"

@Injectable({
  providedIn: "root"
})
export class AuthGuard implements CanActivate{
  constructor(private authService: AuthService, private router: Router, private store: Store<RootState>){}
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean{
    return true
  }
}
