import {Component, Inject, OnInit} from "@angular/core"
import {AuthService} from "../../data/services/auth.service"
import {LoginParams} from "../../data/models/auth.model"
import {MatDialog, MatSnackBar} from "@angular/material"
import {Router} from "@angular/router"
import {Location} from "@angular/common"

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {

  constructor(private snackbar: MatSnackBar, private authService: AuthService, private dialog: MatDialog, private router: Router, private location: Location) { }

  ngOnInit() {
  }

  login(value: any) {
    this.authService.login(
      {
        username: value.username,
        password: value.password
      }
    )
      .subscribe(() => {},
        (error) => {
          this.showDialog("Đăng nhập thất bại", error)
      }, () => {
          this.location.back()
      })
  }
  showDialog(title: string, message: string, onClose?: () => any){
    this.snackbar.open(title + ": " + message, null, {
      duration: 2000
    })
    if(onClose  != null){
      onClose()
    }
  }
}

