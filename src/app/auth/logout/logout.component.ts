import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../data/services/auth.service"
import {Router} from "@angular/router"
import {Location} from "@angular/common"

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  constructor(private authService: AuthService, private router: Router, private location: Location) { }

  ngOnInit() {
    this.authService.logout()
      .subscribe(() => {},() => {}, () => {
        this.router.navigate(["/home"])
      })
  }
}
