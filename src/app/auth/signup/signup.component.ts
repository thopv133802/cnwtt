import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../data/services/auth.service"
import {RegisterParams} from "../../data/models/auth.model"
import {MatDialog, MatSnackBar} from "@angular/material"
import {Router} from "@angular/router"

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  constructor(private snackBar: MatSnackBar, private authService: AuthService, private dialog: MatDialog, private router: Router) { }

  ngOnInit() {
  }
  register(value: any){
    console.log(value)
    this.authService.register(
      {
        username: value.username,
        password: value.password,
        birthday: value.birthday,
        fullName: value.fullName
      }
    )
      .subscribe(() => {},
        (message) => {
          this.showDialog("Đăng ký thất bại", message)
      }, () => {
          this.showDialog("Đăng ký thành công", "Hãy đăng nhập để tiếp tục sử dụng", () => {
            this.router.navigate(["/login"])
          })
      })
  }
  showDialog(title: string, message: string, onClose?: () => any){
    this.snackBar.open(title + ": " + message, null, {
      duration: 2000
    })
    if(onClose != null) {
      onClose()
    }
  }
}
