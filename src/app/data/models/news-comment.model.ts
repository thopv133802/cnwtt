import {fakeNews} from "./news.faker"

export class NewsComment{
  id: string
  content: string
  commenter: Commenter
  created: number
  newsID: string

  constructor(id: string, content: string, commenter: Commenter, created: number, newsID: string) {
    this.id = id
    this.content = content
    this.commenter = commenter
    this.created = created
    this.newsID = newsID
  }
}
export class Commenter{
  id: string
  name: string
  thumbnail: string

  constructor(id: string, name: string, thumbnail: string) {
    this.id = id
    this.name = name
    this.thumbnail = thumbnail
  }
}
