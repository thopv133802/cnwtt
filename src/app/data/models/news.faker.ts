import {News} from "./news.model"


export function fakeNews(): News {
  const number = Date.now()
  return {
    id: number.toString(),
    content: "Thưởng thức món ăn khoái khẩu đôi khi cũng là thú vui nhưng có không ít món ăn cần phải cân nhắc trước khi “đụng đũa” bởi bạn có thể phải trả giá bằng cả mạng sống.\n" +
      "Não có mủ vì các món tái\n" +
      "\n" +
      "Bò, dê tái chanh, gỏi cá, nem chua… là những món ăn được xếp vào hàng thông dụng của người Việt nhưng lại có nguy cơ gây bệnh rất cao, thậm chí dẫn đến tử vong.\n" +
      "\n" +
      "Chuyên gia dinh dưỡng Lê Thị Hải - giám đốc Trung tâm tư vấn Dinh dưỡng - Viện Dinh dưỡng quốc gia cho rằng, nếu đảm bảo được nguồn thực phẩm sạch, không nhiễm khuẩn, giun sán thì những món ăn này hoàn toàn đảm bảo chất dinh dưỡng và tiêu hóa bình thường. Tuy nhiên, ở Việt Nam, nguồn cung cấp thực phẩm thiếu an toàn nên có thể gây hại đến sức khỏe.",
    thumbnail: "https://cdn.eva.vn/upload/4-2018/images/2018-11-27/15998810-2017-09-11112506-1505118246-650-5b9d330179-1505509395-1543289703-691-width800height600.jpg",
    category: "Sống khỏe",
    author: "Theo Dương Dương (Dịch từ Brightside) (Khám phá)",
    creatorId: number.toString(),
    created: number,
    title: "12 món ăn nhiều người thích là kịch độc với sức khỏe, không ít món người Việt ăn nhiều",
    description: "Một số món ăn tưởng như rất an toàn hay thậm chí đặc trưng ở một số nước nhưng chúng lại vô cùng nguy hiểm....",
    creatorName: number.toString()
  }
}

export function fakeNewses() {
  return [
    fakeNews(),
    fakeNews(),
    fakeNews(),
    fakeNews(),
    fakeNews(),
    fakeNews(),
    fakeNews(),
    fakeNews(),
  ]
}
