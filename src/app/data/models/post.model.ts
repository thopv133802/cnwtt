export interface Post{
  id: string,
  title: string,
  content: string,
  authorId: string,
  authorName: string,
  created: number
}

export function fakePost(): Post{
  const currentTimeInMillis = Date.now()
  return {
    id: currentTimeInMillis.toString(),
    title: "Tiêu đề của bài viết",
    content: "Nội dung của bài viết",
    authorId: currentTimeInMillis.toString(),
    authorName: "Tác giả của bài viết",
    created: currentTimeInMillis
  }
}

export interface PostComment{
  id: string,
  content: string,
  authorId:  string,
  authorName: string
  postId: string
  created: number
}
export function fakePostComment(): PostComment{
  const currentTimeInMillis = Date.now()
  return {
    id: currentTimeInMillis.toString(),
    content: "Nội dung bình luận",
    authorId: currentTimeInMillis.toString(),
    authorName: currentTimeInMillis.toString(),
    postId: currentTimeInMillis.toString(),
    created: currentTimeInMillis
  }
}
