export interface News {
  id: string
  title: string
  description: string
  content: string
  thumbnail: string
  category: string
  author: string
  creatorName: string
  creatorId: string
  created: number

}
