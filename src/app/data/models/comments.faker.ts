import {Commenter, NewsComment} from "./news-comment.model"
import {fakeNews} from "./news.faker"


export function fakeComment(): NewsComment{
  const commenter =  fakeCommenter()
  let number = Date.now()
  return new NewsComment(
    number.toString(),
    "Chào",
    commenter,
    number,
     number.toString()
  )
}

function fakeCommenter(): Commenter{
  return new Commenter(
    Date.now().toString(),
    "Tho Pham",
    "https://a.vnecdn.net/avatar/c60x60/2017/06/25/8072536753_1498375073.JPG"
  )
}
