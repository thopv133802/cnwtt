export enum Role{
  Admin,
  Member
}
export interface User{
  username: string
  password: string
  fullName: string
  birthday: string
  role: Role
}
export interface LoginParams{
  username: string
  password: string
}
export interface RegisterParams{
  username: string
  fullName: string
  password: string
  birthday: string
}
