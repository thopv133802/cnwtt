
import {Action} from "@ngrx/store"
import {Post, PostComment} from "../models/post.model"

export const CREATE_POST = "[Forum] CreatePost"
export const UPDATE_POST = "[Forum] UpdatePost"

export class CreatePost implements Action{
  type = CREATE_POST
  post: Post

  constructor(post: Post) {
    this.post = post
  }
}
export class UpdatePost implements Action{
  type = UPDATE_POST
  post: Post

  constructor(post: Post) {
    this.post = post
  }
}
export type PostAction = CreatePost | UpdatePost
