import {PostComment} from "../models/post.model"
import {CREATE_COMMENT, PostCommentAction} from "./post-comment.action"

export interface PostCommentState{
  comments: PostComment[]
}

const initialState = {
  comments: []
}

export function postCommentReducer(state: PostCommentState = initialState, action: PostCommentAction): PostCommentState{
  switch(action.type){
    case CREATE_COMMENT:
      return {
        comments: [
          ...state.comments,
          action.comment
        ]
      }
    default:
      return state
  }
}


export const howToFetchPostComments = (state: PostCommentState) => {
  return state.comments
}
