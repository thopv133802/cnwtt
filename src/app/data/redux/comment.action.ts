import {Action} from "@ngrx/store"
import {NewsComment} from "../models/news-comment.model"

export const ADD_COMMENT = "[comment] add_comment"
export class AddComment implements Action{
  type = ADD_COMMENT
  comment: NewsComment


  constructor(comment: NewsComment) {
    this.comment = comment
  }
}

export type CommentAction = AddComment
