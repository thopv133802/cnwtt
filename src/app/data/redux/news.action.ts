import {Action} from "@ngrx/store"
import {News} from "../models/news.model"

export const ADD_NEWS = "[news] add_news"
export const ADD_NEWSES = "[news] add_newses"
export const UPDATE_NEWS = "[news] update_news"

export class AddNews implements Action{
  type = ADD_NEWS
  news: News
  newses: News[]
  constructor(news: News) {
    this.news = news
  }
}
export class UpdateNews implements Action{
  type = UPDATE_NEWS
  news: News
  newses: News[]
  constructor(news: News) {
    this.news = news
  }
}

export class AddNewses implements Action{
  type = ADD_NEWSES
  newses: News[]
  news:News
  constructor(newses: News[]) {
    this.newses = newses
  }
}

export type NewsAction =  AddNews | UpdateNews | AddNewses
