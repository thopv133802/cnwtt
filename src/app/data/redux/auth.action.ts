import {Action} from "@ngrx/store"
import {User} from "../models/auth.model"

export const SET_AUTHENTICATED = "[Auth] SetAuthenticated"
export const SET_UNAUTHENTICATED = "[Auth] SetUnauthenticated"
export const ADD_USER = "[Auth] AddUser"
export const UPDATE_USER = "[Auth] UpdateUser"
export const SET_JWT_TOKEN = "[Auth] SetJwtToken"

export class SetAuthenticated implements Action{
  readonly type = SET_AUTHENTICATED
  user: User
  token: string

  constructor(user: User) {
    this.user = user
  }
}
export class SetUnauthenticated implements Action{
    readonly type = SET_UNAUTHENTICATED
    user: User
    token: string
}
export class AddUser implements Action{
  readonly type = ADD_USER
  user: User
  token: string

  constructor(user: User) {
    this.user = user
  }
}
export class UpdateUser implements Action{
  readonly type = UPDATE_USER
  user: User
  token: string

  constructor(user: User) {
    this.user = user
  }
}
export class SetJwtToken implements  Action{
  type = SET_JWT_TOKEN
  user: User
  token: string

  constructor(token: string) {
    this.token = token
  }
}
export type AuthAction = SetAuthenticated | SetUnauthenticated | AddUser | UpdateUser | SetJwtToken
