import {ADD_NEWS, ADD_NEWSES, NewsAction, UPDATE_NEWS} from "./news.action"
import {News} from "../models/news.model"
import {fakeNews, fakeNewses} from "../models/news.faker"

export interface NewsState{
  newses: News[]
}
export const initialState: NewsState = {
  newses: [fakeNews()]
}

export function newsReducer(state: NewsState = initialState, action: NewsAction): NewsState{
  switch (action.type) {
    case ADD_NEWS:
      return {
        newses: [action.news, ...state.newses]
      }
    case UPDATE_NEWS:
      return {
        newses: [
          action.news,
          ...state.newses.filter(news => news.id !== action.news.id)
        ]
      }
    case ADD_NEWSES:
      return{
        newses: [
          ...action.newses,
          ...state.newses
        ]
      }
    default:
      return state
  }
}
export const howToFetchNewses = (newsState: NewsState) => {
  return newsState.newses
}
