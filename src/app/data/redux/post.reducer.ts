import {CREATE_POST, CreatePost, PostAction, UPDATE_POST} from "./post.action"
import {fakePost, Post, PostComment} from "../models/post.model"

export interface PostState{
  posts: Post[]
}
export const initialState = {
  posts: [
    fakePost()
  ]
}

export function postReducer(state: PostState = initialState, action: PostAction): PostState{
  switch (action.type) {
    case CREATE_POST:
      return {
        posts: [
          action.post,
          ...state.posts
        ]
      }
    case UPDATE_POST:
      return {
        posts: [
          ...state.posts.filter(post =>
            post.id === action.post.id
          ).map(post => {
            return action.post
          }),
          ...state.posts.filter(post =>
            post.id !== action.post.id
          )
        ]
      }
    default:
      return state
  }
}

export const howToFetchPosts = (state: PostState) => {
  return state.posts
}
