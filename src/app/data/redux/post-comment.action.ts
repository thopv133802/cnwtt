import {Action} from "@ngrx/store"
import {PostComment} from "../models/post.model"

export const CREATE_COMMENT = "[Forum] CreateComment"


export class CreateComment implements Action{
  type = CREATE_COMMENT
  comment: PostComment

  constructor(comment: PostComment) {
    this.comment = comment
  }
}

export type PostCommentAction = CreateComment
