import {
  ADD_USER,
  AuthAction,
  SET_AUTHENTICATED, SET_JWT_TOKEN,
  SET_UNAUTHENTICATED,
  SetAuthenticated,
  SetUnauthenticated,
  UPDATE_USER
} from "./auth.action"
import {Role, User} from "../models/auth.model"
import {fetchAuthState} from "../../app.reducer"

export interface AuthState{
  isAuth: boolean
  currentUser: User
  users:  User[]
  token: string
}

const users = [
  {
    username: "jlaotsezu",
    password: "123456",
    fullName: "Laotsezu",
    role: Role.Admin,
    birthday: "Sat Dec 02 1995 00:00:00 GMT+0700 (Indochina Time)"
  },{
    username: "thopvna",
    password: "123456",
    fullName: "Thọ Phạm",
    role: Role.Member,
    birthday: "Sat Dec 02 1995 00:00:00 GMT+0700 (Indochina Time)"
  }
]

const jwtToken = "Bearer abcxyz_"

export const initialState = {
  isAuth: false,
  currentUser: null,
  users: users,
  token: null
}

export function authReducer(state: AuthState = initialState, action: AuthAction): AuthState{
  console.log("On AUth Reducer with action = " + action.type)
  switch(action.type){
    case SET_AUTHENTICATED:
      return {
        isAuth: true,
        currentUser: action.user,
        users: [...state.users],
        token: state.token
      }
    case SET_UNAUTHENTICATED:
      return {
        isAuth: false,
        currentUser: null,
        users: [...state.users],
        token: state.token
      }
    case ADD_USER:
      return {
        isAuth: state.isAuth,
        currentUser: state.currentUser,
        users: [action.user, ...state.users],
        token: state.token
      }
    case UPDATE_USER:
      return {
        isAuth: state.isAuth,
        currentUser: state.currentUser,
        users: [
          action.user,
          ...state.users.filter(user => user.username !== action.user.username)
        ],
        token: state.token
      }
    case SET_JWT_TOKEN:
      return{
        isAuth: state.isAuth,
        currentUser: state.currentUser,
        users: state.users,
        token: action.token
      }
    default:
      return state
  }
}

export const howToFetchIsAuth = (authState: AuthState) => {
  return authState.isAuth
}
export const howToFetchAuthUser = (authState: AuthState) => {
  return authState.currentUser
}
export const howToFetchUsers = (authState: AuthState) => {
  return authState.users
}
