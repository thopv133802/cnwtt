import {fakeComment} from "../models/comments.faker"
import {NewsComment} from "../models/news-comment.model"
import {ADD_COMMENT, AddComment, CommentAction} from "./comment.action"

export interface CommentState{
  comments: NewsComment[]
}

const initialState: CommentState = {
  comments: []
}

export function commentReducer(state: CommentState = initialState, action: CommentAction): CommentState{
  switch (action.type) {
    case ADD_COMMENT:
      return {
        comments: [
          ...state.comments,
          action.comment
        ]
      }
    default:
      return state
  }
}

export const howToFetchComments = (commentState: CommentState) => {
  console.log(commentState.comments)
  return commentState.comments
}
