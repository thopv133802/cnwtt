import {Injectable, OnInit} from "@angular/core"
import {News} from "../models/news.model"
import {fetchCurrentUser, fetchNewses, RootState} from "../../app.reducer"
import {select, Store} from "@ngrx/store"
import {EMPTY, Observable, of, throwError} from "rxjs"
import {filter, first, flatMap, map, single} from "rxjs/operators"
import {AddNews, UpdateNews} from "../redux/news.action"
import {fakeNews} from "../models/news.faker"
import {AuthService} from "./auth.service"

@Injectable({
  providedIn: 'root'
})
export class NewsService implements OnInit{
  constructor(private store: Store<RootState>, private authService: AuthService) { }

  ngOnInit(): void {
  }

  fetchNews(newsID: string): Observable<News> {
    return this.fetchNewses().pipe(
      map(newses => {
        return newses.find(news => {
          return news.id === newsID
        })
      })
    )
  }

  fetchRelatedNewses(newsID: string): Observable<News[]> {
    return this.fetchNewses().pipe(
      map(newses => {
        return newses.filter(news => {
          return news.id !== newsID
        })
      })
    )
  }

  fetchNewses(): Observable<News[]> {
    return this.store.pipe(
      select(fetchNewses)
    )
  }

  addNews(params: AddNewsParams): Observable<never>{
    return this.store.pipe(
      select(fetchCurrentUser)
    )
      .pipe(
        first()
      )
      .pipe(
        flatMap(currentUser => {
          if (currentUser == null)  {
            return throwError("Người dùng hiện tại không khả dụng")
          }
          else {
            const news = fakeNews()
            news.title = params.title
            news.content = params.content
            news.thumbnail = params.thumbnail
            news.author = params.author
            news.creatorName = currentUser.username
            news.creatorId = currentUser.username
            this.store.dispatch(new AddNews(
              news
            ))
            return EMPTY
          }
        })
      )
  }

  updateNews(news: News): Observable<never>{
    this.store.dispatch(
      new UpdateNews(
        news
      )
    )
    return EMPTY
  }
}
export interface AddNewsParams{
  title: string,
  content: string,
  thumbnail: string,
  author: string
}
