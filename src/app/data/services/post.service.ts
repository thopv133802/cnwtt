import { Injectable } from '@angular/core';
import {fetchPostComments, fetchPosts, RootState} from "../../app.reducer"
import {select, Store} from "@ngrx/store"
import {EMPTY, Observable, throwError} from "rxjs"
import {fakePost, fakePostComment, Post, PostComment} from "../models/post.model"
import {CreatePost, UpdatePost} from "../redux/post.action"
import {AuthService} from "./auth.service"
import {filter, first, flatMap, map} from "rxjs/operators"
import {CreateComment} from "../redux/post-comment.action"

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private store: Store<RootState>, private authService: AuthService) {

  }
  fetchPosts(): Observable<Post[]>{
    return this.store.pipe(
      select(fetchPosts)
    )
  }
  createPost(params: CreatePostParams): Observable<never>{
    return this.authService.fetchCurrentUser()
      .pipe(
        first()
      )
      .pipe(
        flatMap(user => {
          if(user == null){
            return throwError("Người dùng hiện tại không khả dụng")
          }
          else {
            const post = fakePost()
            post.title = params.title
            post.content = params.content
            post.authorId = user.username
            post.authorName = user.username
            this.store.dispatch(
              new CreatePost(
                post
              )
            )
            return EMPTY
          }
        })
      )
  }

  createComment(params: CreatePostCommentParams): Observable<never>{
    return this.authService.fetchCurrentUser()
      .pipe(first())
      .pipe(
        flatMap(user => {
          if(user == null) {
            return throwError("Người dùng hiện tại không khả dụng")
          }
          else {
            const comment = fakePostComment()
            comment.content = params.content
            comment.postId = params.postId
            comment.authorId = user.username
            comment.authorName = user.username
            this.store.dispatch(
              new CreateComment(
                comment
              )
            )
            return EMPTY
          }
        })
      )
  }

  fetchPost(postID: string): Observable<Post> {
    return this.fetchPosts()
      .pipe(
        map(posts => posts.find(post => post.id === postID))
      )
  }

  fetchComments(postID: string): Observable<PostComment[]> {
    return this.store.pipe(
      select(fetchPostComments)
    )
      .pipe(
        map(comments  => comments.filter(comment => comment.postId === postID))
      )
  }

  fetchRelatedPosts(postID: string): Observable<Post[]> {
    return this.fetchPosts()
      .pipe(
        map(posts => posts.filter(post => post.id !== postID))
      )
  }

  updatePost(post: Post): Observable<Post> {
    this.store.dispatch(
      new UpdatePost(post)
    )
    return EMPTY
  }
}
export interface CreatePostCommentParams{
  content: string
  postId: string
}
export interface CreatePostParams{
  title: string
  content: string
}
