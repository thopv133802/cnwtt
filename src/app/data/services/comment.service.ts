import {Injectable, OnInit} from "@angular/core"
import {Commenter, NewsComment} from "../models/news-comment.model"
import {EMPTY, Observable, throwError} from "rxjs"
import {filter, first, flatMap, map} from "rxjs/operators"
import {fetchComments, fetchCurrentUser, fetchUsers, RootState} from "../../app.reducer"
import {select, Store} from "@ngrx/store"
import {AddComment} from "../redux/comment.action"
import {fakeComment} from "../models/comments.faker"
import {AuthService} from "./auth.service"
@Injectable({
  providedIn: "root"
})
export class CommentService implements OnInit{
  constructor(private store: Store<RootState>, private authService: AuthService) {

  }
  ngOnInit(): void {
  }

  fetchComments(newsID: string): Observable<NewsComment[]> {
    return this.store.pipe(
      select(fetchComments)
    ).pipe(
      map(comments => comments.filter(comment => {
        return comment.newsID === newsID
      }))
    )
  }

  addComment(params: AddCommentParams): Observable<never> {
    return this.store.pipe(
      select(fetchCurrentUser)
    )
      .pipe(
        first()
      )
      .pipe()
      .pipe(
        flatMap(user => {
          if (user == null) {
            return throwError("Người dùng hiện tại không khả dụng")
          } else {
            const comment = fakeComment()
            comment.newsID = params.newsId
            comment.content = params.content
            comment.commenter.id = user.username
            comment.commenter.name = user.username
            this.store.dispatch(new AddComment(
              comment
            ))
            return EMPTY
          }
        })
      )
  }
}

export class AddCommentParams{
  newsId: string
  content: string

  constructor(newsId: string, content: string) {
    this.newsId = newsId
    this.content = content
  }
}
