import {Injectable, OnInit} from "@angular/core"
import {LoginParams, RegisterParams, Role, User} from "../models/auth.model"
import {EMPTY, Observable, throwError} from "rxjs"
import {select, Store} from "@ngrx/store"
import {AddUser, SetAuthenticated, SetUnauthenticated, UpdateUser} from "../redux/auth.action"
import {fetchCurrentUser, fetchIsAuth, fetchUsers, RootState} from "../../app.reducer"
import {first, flatMap, map} from "rxjs/operators"

@Injectable({
  providedIn: 'root'
})
export class AuthService implements OnInit{
  constructor(private store: Store<RootState>) {
  }
  ngOnInit(): void {
  }

  login(params: LoginParams): Observable<never>{
    return this.store.pipe(
      select(fetchUsers)
    )
      .pipe(
        first()
      )
      .pipe(
        map(users => users.find(user => user.username === params.username && user.password ===params.password ))
      )
      .pipe(
        flatMap(user => {
          if(user == null){
            return throwError("Tài khoản hoặc mật khẩu không  chính xác")
          }
          else{
            this.store.dispatch(
              new SetAuthenticated(
                user
              )
            )
            return EMPTY
          }
        })
      )
  }
  register(params: RegisterParams): Observable<never>{
    return this.store.pipe(
      select(fetchUsers)
    )
      .pipe(
        first()
      )
      .pipe(
        map(users => users.find(user => {
          return user.username === params.username
        }))
      )
      .pipe(
        flatMap(user => {
          console.log(user)
          if(user == null){
            this.store.dispatch(
              new AddUser(
                {
                  username:  params.username,
                  password: params.password,
                  birthday: params.birthday,
                  fullName: params.fullName,
                  role: Role.Member
                }
              )
            )
            return EMPTY
          }
          else{
            return throwError("Tài khoản đã có người sử dụng")
          }
        })
      )
  }
  logout(): Observable<void>{
    this.store.dispatch(new SetUnauthenticated())
    return EMPTY
  }

  fetchCurrentUser(): Observable<User> {
    return this.store.pipe(
      select(fetchCurrentUser)
    )
  }

  fetchIsAuth(): Observable<boolean>{
    return this.store.pipe(
      select(fetchIsAuth)
    )
  }

  updateUser(user: User): Observable<never> {
    this.store.dispatch(
      new UpdateUser(user)
    )
    return EMPTY
  }
}
