import {ActionReducerMap, createFeatureSelector, createSelector} from "@ngrx/store"
import {authReducer, AuthState, howToFetchAuthUser, howToFetchIsAuth, howToFetchUsers} from "./data/redux/auth.reducer"
import {howToFetchNewses, newsReducer, NewsState} from "./data/redux/news.reducer"
import {commentReducer, CommentState, howToFetchComments} from "./data/redux/comment.reducer"
import {howToFetchPosts, postReducer, PostState} from "./data/redux/post.reducer"
import {howToFetchPostComments, postCommentReducer, PostCommentState} from "./data/redux/post-comment.reducer"

export interface RootState{
  auth: AuthState,
  news: NewsState,
  comment: CommentState,
  post: PostState,
  postComment: PostCommentState
}

export const reducers: ActionReducerMap<RootState> = {
  auth: authReducer,
  news: newsReducer,
  comment: commentReducer,
  post: postReducer,
  postComment: postCommentReducer
}

export const fetchAuthState = createFeatureSelector<AuthState>("auth")
export const fetchIsAuth = createSelector(fetchAuthState, howToFetchIsAuth)
export const fetchCurrentUser = createSelector(fetchAuthState, howToFetchAuthUser)
export const fetchUsers = createSelector(fetchAuthState, howToFetchUsers)

export const fetchNewsState = createFeatureSelector<NewsState>("news")
export const fetchNewses = createSelector(fetchNewsState, howToFetchNewses)

export const fetchCommentState = createFeatureSelector("comment")
export const fetchComments = createSelector(fetchCommentState, howToFetchComments)

export const fetchPostState = createFeatureSelector("post")
export const fetchPosts = createSelector(fetchPostState, howToFetchPosts)

export const fetchPostCommentReducer = createFeatureSelector("postComment")
export const fetchPostComments = createSelector(fetchPostCommentReducer, howToFetchPostComments)
