import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatGridListModule,
  MatCardModule,
  MatMenuModule,
  MatIconModule,
  MatButtonModule,
  MatToolbarModule,
  MatSidenavModule,
  MatListModule,
  MatFormFieldModule,
  MatInputModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatCheckboxModule,
  MatRadioModule,
  MatDialogModule,
  MatProgressBarModule, MatSnackBarModule
} from "@angular/material"
import { LayoutModule } from '@angular/cdk/layout';
import { AppRoutingModule } from './app-routing.module';
import { LoginComponent } from './auth/login/login.component';
import { SignupComponent } from './auth/signup/signup.component';
import {FlexLayoutModule} from "@angular/flex-layout"
import {FormsModule} from "@angular/forms";
import { SidenavListComponent } from './navigation/sidenav-list/sidenav-list.component';
import { AppbarComponent } from './navigation/appbar/appbar.component';
import { HomeComponent } from './home/home.component';
import { NewsCardComponent } from './home/news-card/news-card.component';
import { HorizontalNewsesComponent } from './home/horizontal-newses/horizontal-newses.component';
import { VerticalNewsesComponent } from './home/vertical-newses/vertical-newses.component';
import { NewsDetailComponent } from './news-detail/news-detail.component';
import { LogoutComponent } from './auth/logout/logout.component';
import { ForumComponent } from './forum/forum.component';
import { CommentCardComponent } from './news-detail/comment-card/comment-card.component';
import { CommentsComponent } from './news-detail/comments/comments.component';
import { AdminComponent } from './admin/admin.component';
import { StoreModule } from '@ngrx/store';
import {reducers} from "./app.reducer";
import { CreateNewsComponent } from './admin/create-news/create-news.component';
import { NewsComponent } from './admin/news/news.component';
import { EditNewsComponent } from './admin/edit-news/edit-news.component';
import { CreatePostComponent } from './forum/create-post/create-post.component';
import { PostComponent } from './forum/post/post.component';
import { PostDetailComponent } from './forum/post-detail/post-detail.component';
import { PostCommentComponent } from './forum/post-comment/post-comment.component';
import { PostCommentsComponent } from './forum/post-comments/post-comments.component';
import { ProfileComponent } from './home/profile/profile.component'
import {HttpClientModule} from "@angular/common/http"

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignupComponent,
    SidenavListComponent,
    AppbarComponent,
    HomeComponent,
    NewsCardComponent,
    HorizontalNewsesComponent,
    VerticalNewsesComponent,
    NewsDetailComponent,
    LogoutComponent,
    ForumComponent,
    CommentCardComponent,
    CommentsComponent,
    AdminComponent,
    CreateNewsComponent,
    NewsComponent,
    EditNewsComponent,
    CreatePostComponent,
    PostComponent,
    PostDetailComponent,
    PostCommentComponent,
    PostCommentsComponent,
    ProfileComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    LayoutModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    AppRoutingModule,
    FlexLayoutModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatCheckboxModule,
    MatRadioModule,
    MatDialogModule,
    MatProgressBarModule,
    StoreModule.forRoot(reducers),
    MatSnackBarModule,
    HttpClientModule
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
