import {Component, Input, OnInit} from "@angular/core"
import {NewsComment} from "../../data/models/news-comment.model"

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.css']
})
export class CommentsComponent implements OnInit {
  @Input()
  comments: NewsComment[]

  constructor() { }

  ngOnInit() {
  }

}
