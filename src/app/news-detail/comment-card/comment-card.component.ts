import {Component, Input, OnInit} from "@angular/core"
import {NewsComment} from "../../data/models/news-comment.model"

@Component({
  selector: 'app-comment-card',
  templateUrl: './comment-card.component.html',
  styleUrls: ['./comment-card.component.css']
})
export class CommentCardComponent implements OnInit {
  @Input()
  comment: NewsComment

  constructor() { }

  ngOnInit() {
  }

}
