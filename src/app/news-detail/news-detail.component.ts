import {Component, OnDestroy, OnInit, SimpleChanges} from "@angular/core"
import {Location} from "@angular/common"
import {NewsService} from "../data/services/news.service"
import {ActivatedRoute, Router} from "@angular/router"
import {News} from "../data/models/news.model"
import {AddCommentParams, CommentService} from "../data/services/comment.service"
import {NewsComment} from "../data/models/news-comment.model"
import {AuthService} from "../data/services/auth.service"
import {Observable} from "rxjs"
import {fetchCurrentUser, fetchIsAuth, RootState} from "../app.reducer"
import {select, Store} from "@ngrx/store"
import {MatSnackBar} from "@angular/material"

@Component({
  selector: 'app-news-detail',
  templateUrl: './news-detail.component.html',
  styleUrls: ['./news-detail.component.css']
})
export class NewsDetailComponent implements OnInit{
  news: News
  otherNewses: News[]
  comments: NewsComment[]
  newCommentContent: string
  isAuth$: Observable<boolean>
  constructor(private snackbar: MatSnackBar, private store: Store<RootState>, private authService: AuthService, private location: Location, private newsService: NewsService, private activatedRoute: ActivatedRoute, private router: Router, private commentService: CommentService) { }

  ngOnInit() {
    const newsID = this.activatedRoute.snapshot.paramMap.get("id")
    this.newsService.fetchNews(newsID).subscribe(news => {
      this.news = news
    })
    this.newsService.fetchRelatedNewses(newsID).subscribe(newses => {
      this.otherNewses = newses
    })

    this.commentService.fetchComments(newsID).subscribe(comments => {
      this.comments = comments
    })
    this.isAuth$ = this.store.pipe(select(fetchIsAuth))

  }

  navigateToDetail($event: string) {
    console.log($event)
    this.router.navigate(["/news/", $event])
  }

  comment() {
    this.commentService.addComment(
      new AddCommentParams(
        this.news.id,
        this.newCommentContent
      )
    ).subscribe(null, (error) => {
      this.showMessage(error)
    }, () => {
      this.showMessage("Thêm bình luận thành công")
      this.newCommentContent = null
    })
  }
  showMessage(message: string){
    this.snackbar.open(message, null, {
      duration: 2000
    })
  }
  navigateToLogin() {
    this.router.navigate(["/login"])
  }
}
