import { Component, OnInit } from '@angular/core';
import {PostService} from "../../data/services/post.service"
import {MatSnackBar} from "@angular/material"
import {first} from "rxjs/operators"
import {Router} from "@angular/router"

@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.css']
})
export class CreatePostComponent implements OnInit {
  titleText = "Limit text length to n lines using CSS"
  contentText = "Is it possible to limit a text length to \"n\" lines using CSS (or cut it when overflows vertically).\n" +
    "\n" +
    "text-overflow: ellipsis; only works for 1 line text.\n" +
    "\n" +
    "original text:\n" +
    "\n" +
    "Ultrices natoque mus mattis, aliquam, cras in pellentesque\n" +
    "tincidunt elit purus lectus, vel ut aliquet, elementum nunc\n" +
    "nunc rhoncus placerat urna! Sit est sed! Ut penatibus turpis\n" +
    "mus tincidunt! Dapibus sed aenean, magna sagittis, lorem velit\n" +
    "\n" +
    "wanted output (2 lines):\n" +
    "\n" +
    "Ultrices natoque mus mattis, aliquam, cras in pellentesque\n" +
    "tincidunt elit purus lectus, vel ut aliquet, elementum..."
  constructor(private postService: PostService, private snackbar: MatSnackBar, private router: Router) { }

  ngOnInit() {
  }

  onCreatePostButtonClicked() {
    this.postService.createPost(
      {
        title: this.titleText,
        content: this.contentText
      }
    ).subscribe(null, error => {
      this.showMessage(error)
    }, () => {
      this.showMessage("Đăng bài viết thành công")
      this.clearFormFields()
    })
  }
  showMessage(message: string){
    this.snackbar.open(message, "Diễn đàn", {
      duration: 2000
    }).onAction().pipe(first())
      .subscribe(() => {
        this.router.navigate(["/forum"])
      })
  }

  private clearFormFields() {
    this.titleText = null
    this.contentText = null
  }
}
