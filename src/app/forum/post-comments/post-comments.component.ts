import {Component, Input, OnInit} from "@angular/core"
import {PostComment} from "../../data/models/post.model"

@Component({
  selector: 'app-post-comments',
  templateUrl: './post-comments.component.html',
  styleUrls: ['./post-comments.component.css']
})
export class PostCommentsComponent implements OnInit {
  @Input()
  comments: PostComment[]
  constructor() { }

  ngOnInit() {
  }

}
