import { Component, OnInit } from '@angular/core';
import {Post} from "../data/models/post.model"
import {Router} from "@angular/router"
import {Role, User} from "../data/models/auth.model"
import {AuthService} from "../data/services/auth.service"
import {PostService} from "../data/services/post.service"

@Component({
  selector: 'app-forum',
  templateUrl: './forum.component.html',
  styleUrls: ['./forum.component.css']
})
export class ForumComponent implements OnInit {
  searchedPosts: Post[] = []
  posts: Post[] = []
  currentUser: User
  role = Role.Admin

  constructor(private router: Router, private authService: AuthService, private postService: PostService) { }

  ngOnInit() {
    this.authService.fetchCurrentUser().subscribe(user => {
      this.currentUser = user
    })
    this.postService.fetchPosts().subscribe(posts => {
      this.posts = posts
    })
  }

  onSearchButtonClick(keyword: string) {
    this.searchedPosts = this.posts.filter(post => {
      return post.title.toLowerCase().includes(keyword.toLowerCase()) || post.content.toLowerCase().includes(keyword.toLowerCase())
    })
  }

  onDetailButtonClicked(postID: string) {
    this.router.navigate(["/post-detail/", postID])
  }

  navigateToCreatePost() {
    this.router.navigate(["/create-post"])
  }
}
