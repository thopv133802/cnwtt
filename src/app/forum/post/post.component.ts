import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core"
import {Post} from "../../data/models/post.model"

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {
  @Input()
  post: Post
  @Output()
  detailButtonClickListener = new EventEmitter()
  constructor() { }

  ngOnInit() {
  }

  onDetailButtonClicked() {
    this.detailButtonClickListener.next()
  }
}
