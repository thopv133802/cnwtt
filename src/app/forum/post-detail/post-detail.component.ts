import {Component, Input, OnInit} from "@angular/core"
import {Post, PostComment} from "../../data/models/post.model"
import {Observable} from "rxjs"
import {PostService} from "../../data/services/post.service"
import {AuthService} from "../../data/services/auth.service"
import {ActivatedRoute, Router} from "@angular/router"
import {MatSnackBar} from "@angular/material"
import {Role} from "../../data/models/auth.model"

@Component({
  selector: 'app-post-detail',
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.css']
})
export class PostDetailComponent implements OnInit {
  post: Post
  comments: PostComment[]
  newCommentContent: string
  isAuth$: Observable<boolean>
  relatedPosts: Post[]
  currentUserIsAdmin = false

  constructor(private snackbar: MatSnackBar, private route: ActivatedRoute, private router: Router, private postService: PostService, private authService: AuthService) { }

  ngOnInit() {
    this.isAuth$ = this.authService.fetchIsAuth()

    const postID = this.route.snapshot.paramMap.get("id")

    this.postService.fetchPost(postID).subscribe(post => {
      this.post = post
    })
    this.postService.fetchComments(postID).subscribe(comments => {
      this.comments = comments
    })
    this.postService.fetchRelatedPosts(postID).subscribe(posts => {
      this.relatedPosts = posts
    })

    this.authService.fetchCurrentUser().subscribe(user => {
      this.currentUserIsAdmin = user.role === Role.Admin
    })
  }

  comment() {
    this.postService.createComment({
      content: this.newCommentContent,
      postId: this.post.id
    }).subscribe(null, error => {
      this.showMessage(error)
    }, () => {
      this.showMessage("Bình luận thành công")
      this.newCommentContent = null
    })
  }

  navigateToLogin() {
    this.router.navigate(["/login"])
  }

  private showMessage(message: string) {
    this.snackbar.open(message, null, {
      duration: 2000
    })
  }

  onEditPostButtonClicked() {
    this.postService.updatePost(this.post)
      .subscribe(null, error => {
        this.showMessage(error)
      }, () => {
        this.showMessage("Cập nhật thành công")
      })
  }
}
