import {Injectable, NgModule} from "@angular/core"
import {ActivatedRouteSnapshot, CanActivate, RouterModule, RouterStateSnapshot, Routes} from "@angular/router"
import {SignupComponent} from "./auth/signup/signup.component"
import {LoginComponent} from "./auth/login/login.component"
import {HomeComponent} from "./home/home.component"
import {NewsDetailComponent} from "./news-detail/news-detail.component"
import {LogoutComponent} from "./auth/logout/logout.component"
import {ForumComponent} from "./forum/forum.component"
import {AdminComponent} from "./admin/admin.component"
import {AuthGuard} from "./auth/auth.guard"
import {Observable} from "rxjs"
import {fetchCurrentUser, fetchIsAuth, RootState} from "./app.reducer"
import {select, Store} from "@ngrx/store"
import {map} from "rxjs/operators"
import {Role} from "./data/models/auth.model"
import {EditNewsComponent} from "./admin/edit-news/edit-news.component"
import {CreateNewsComponent} from "./admin/create-news/create-news.component"
import {CreatePostComponent} from "./forum/create-post/create-post.component"
import {PostDetailComponent} from "./forum/post-detail/post-detail.component"
import {ProfileComponent} from "./home/profile/profile.component"


@Injectable()
export class IsAdminGuard implements CanActivate{
  constructor(private store: Store<RootState>){}
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.store.pipe(
      select(fetchCurrentUser)
    ).pipe(
      map(user => {
        return user == null ? false : user.role === Role.Admin
      })
    )
  }

}
@Injectable()
export class IsLoggedGuard implements  CanActivate{
  constructor(private store: Store<RootState>){}
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.store.pipe(
      select(fetchIsAuth)
    )
  }
}
@Injectable()
export class IsLogoutGuard implements  CanActivate{
  constructor(private store: Store<RootState>){}
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.store.pipe(
      select(fetchIsAuth)
    ).pipe(
      map(isAuth => {
          return !isAuth
      })
    )
  }
}


export const routes: Routes = [
  { path: "home", component: HomeComponent},
  { path: "", redirectTo: "/home", pathMatch: "full"},
  { path: "signup", component: SignupComponent, canActivate: [IsLogoutGuard]},
  {path: "login", component: LoginComponent, canActivate: [IsLogoutGuard]},
  {path: "news/:id", component: NewsDetailComponent},
  {path: "logout", component: LogoutComponent, canActivate: [IsLoggedGuard]},
  {path: "forum", component: ForumComponent, canActivate: [IsLoggedGuard]},
  {path: "admin", component: AdminComponent, canActivate: [IsAdminGuard]},
  {path: "edit-news/:id", component: EditNewsComponent, canActivate: [IsAdminGuard]},
  {path: "create-news", component: CreateNewsComponent, canActivate: [IsAdminGuard]},
  {path: "create-post", component: CreatePostComponent, canActivate: [IsAdminGuard]},
  {path: "post-detail/:id", component: PostDetailComponent, canActivate: [IsLoggedGuard]},
  {path: "profile", component: ProfileComponent, canActivate: [IsLoggedGuard]},
]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes, {onSameUrlNavigation: "reload"})
  ],
  exports: [
    RouterModule
  ],
  providers: [IsAdminGuard, IsLoggedGuard, IsLogoutGuard]
})
export class AppRoutingModule{

}
